const iterateUpArray = require('@amphibian/iterate-up-array');
const iterateDownArray = require('@amphibian/iterate-down-array');
const isFunction = require('@amphibian/is-function');

/**
 * Create emitter
 * @returns {Object} emitter
**/
function createEmitter() {
    const listeners = [];
    let propagationStopped = false;

    return {
        emit(...args) {
            propagationStopped = false;

            iterateUpArray(listeners.slice(), (listener) => {
                if (listener && !propagationStopped) {
                    listener(...args);
                }
            });
        },

        on(listener) {
            if (!listener || !isFunction(listener)) {
                throw new Error('`listener` must be a function');
            }

            listeners.push(listener);
            return this.off.bind(null, listener);
        },

        off(listener) {
            if (!listener || !isFunction(listener)) {
                throw new Error('`listener` must be a function');
            }

            iterateDownArray(listeners, (existingListener, i) => {
                if (existingListener === listener) {
                    listeners.splice(i, 1);
                }
            });
        },

        stopPropagation() {
            propagationStopped = true;
        }
    };
}

module.exports = createEmitter;
