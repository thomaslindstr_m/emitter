'use strict';

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var iterateUpArray = require('@amphibian/iterate-up-array');
var iterateDownArray = require('@amphibian/iterate-down-array');
var isFunction = require('@amphibian/is-function');

/**
 * Create emitter
 * @returns {Object} emitter
**/
function createEmitter() {
    var listeners = [];
    var propagationStopped = false;

    return {
        emit: function emit() {
            for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            propagationStopped = false;

            iterateUpArray(listeners.slice(), function (listener) {
                if (listener && !propagationStopped) {
                    listener.apply(undefined, _toConsumableArray(args));
                }
            });
        },
        on: function on(listener) {
            if (!listener || !isFunction(listener)) {
                throw new Error('`listener` must be a function');
            }

            listeners.push(listener);
            return this.off.bind(null, listener);
        },
        off: function off(listener) {
            if (!listener || !isFunction(listener)) {
                throw new Error('`listener` must be a function');
            }

            iterateDownArray(listeners, function (existingListener, i) {
                if (existingListener === listener) {
                    listeners.splice(i, 1);
                }
            });
        },
        stopPropagation: function stopPropagation() {
            propagationStopped = true;
        }
    };
}

module.exports = createEmitter;
