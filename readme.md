# emitter

small utility emitter library

```
npm install @amphibian/emitter
```

```javascript
const createEmitter = require('@amphibian/emitter');
const myCustomEmitter = createEmitter();

myCustomEmitter.on((data) => {
    console.log(data);
});

myCustomEmitter.emit('Hello');
// > 'Hello'
```

## Unsubscribe

The following example will only trigger `console.log` once as the event listener unsubscribes itself the first time it is fired.

```javascript
const myCustomEmitter = createEmitter();
const unsubscribe = myCustomEmitter.on((data) => {
    unsubscribe();
    console.log(data);
});

myCustomEmitter.emit('Hello');
myCustomEmitter.emit('Hello');
// > 'Hello'
```

You can also save the function and call `off`.

```javascript
const myCustomEmitter = createEmitter();
const eventHandler = (data) => {
    console.log(data);
};

myCustomEmitter.on(eventHandler);
myCustomEmitter.off(eventHandler);
```

## Stop propagation

To prevent subsequent listeners from firing, stop propagation as follows:

```javascript
const myCustomEmitter = createEmitter();

myCustomEmitter.on(() => {
    myCustomEmitter.stopPropagation();
    console.log('I am number one.');
});

myCustomEmitter.on(() => {
    console.log('I am number two.');
});

myCustomEmitter.emit();
// > 'I am number one.'
```

Only the first event listener will be called.
