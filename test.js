const createEmitter = require('./index');

test('should be able to create an emitter', () => {
    createEmitter();
});

test('should be able to emit with no listeners', () => {
    const emitter = createEmitter();
    emitter.emit();
});

test('should be able to stop propagation with no listeners', () => {
    const emitter = createEmitter();
    emitter.stopPropagation();
});

test('should be able to register a listener', () => {
    const emitter = createEmitter();
    emitter.on(() => {});
});

test('should throw if listener is not provided', () => {
    const emitter = createEmitter();

    try {
        emitter.on();
    } catch (error) {
        expect(error.message).toBe('`listener` must be a function');
    }
});

test('should throw if listener is not a function', () => {
    const emitter = createEmitter();

    try {
        emitter.on([]);
    } catch (error) {
        expect(error.message).toBe('`listener` must be a function');
    }
});

test('should be able to unregister a listener', () => {
    const emitter = createEmitter();
    emitter.off(() => {});
});

test('should throw if listener is not provided when unregistering', () => {
    const emitter = createEmitter();

    try {
        emitter.off();
    } catch (error) {
        expect(error.message).toBe('`listener` must be a function');
    }
});

test('should throw if listener is not a function when unregistering', () => {
    const emitter = createEmitter();

    try {
        emitter.off([]);
    } catch (error) {
        expect(error.message).toBe('`listener` must be a function');
    }
});

test('should call listener when emitted', () => {
    const emitter = createEmitter();
    let value = false;

    emitter.on(() => {
        value = true;
    });

    emitter.emit();
    expect(value).toBe(true);
});

test('should call multiple listeners when emitted', () => {
    const emitter = createEmitter();
    let value = 1;

    emitter.on(() => {
        value += 1;
    });

    emitter.on(() => {
        value += 1;
    });

    emitter.emit();
    expect(value).toBe(3);
});

test('should not call listener after unsubscribing', () => {
    const emitter = createEmitter();
    let value = false;

    const unsubscribe = emitter.on(() => {
        value = true;
    });

    unsubscribe();
    emitter.emit();

    expect(value).toBe(false);
});

test('should not unsubscribe other listeners', () => {
    const emitter = createEmitter();
    let value = 1;

    const unsubscribe = emitter.on(() => {
        value = 2;
    });

    emitter.on(() => {
        value = 3;
    });

    unsubscribe();
    emitter.emit();

    expect(value).toBe(3);
});

test('should not call subsequent listeners after propagation is stopped', () => {
    const emitter = createEmitter();
    let value = 'a';

    emitter.on(() => {
        value += 'b';
    });

    emitter.on(() => {
        emitter.stopPropagation();
        value += 'c';
    });

    emitter.on(() => {
        value += 'd';
    });

    emitter.on(() => {
        value += 'e';
    });

    emitter.emit();
    expect(value).toBe('abc');
});

test('should call subsequent listeners after one unsubscribes', () => {
    const emitter = createEmitter();
    let value = 'a';

    const unsubscribe = emitter.on(() => {
        unsubscribe();
        value += 'b';
    });

    emitter.on(() => {
        value += 'c';
    });

    emitter.emit();
    expect(value).toBe('abc');
});
